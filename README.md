Description
===========

**pythonrestfulAPI.py** - It runs restful webservice which uses python flask webframework and Sqlite3 database

**database.py** - It creates dummy sqlite3 database(employeeschema.db) & insert a sample record.

**employeeschema.db** - will be created under /tmp directory

**template/** - Contains User Interface to view, store, edit, cpuinfo
